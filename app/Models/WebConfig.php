<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebConfig extends Model
{
    protected $fillable = [
        'irrigate_interval',
        'lastIrrigationTime',
        'dayState'
    ];

    public $timestamps = false;
}
