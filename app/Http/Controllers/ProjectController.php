<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data;
use App\Models\IrrigationState;
use App\Models\WebConfig;
use DB;

class ProjectController extends Controller
{
    private $h_increment = [
        'hI' => 0,
        'mI' => 0, 
    ];

    public function __construct() {
        date_default_timezone_set('America/Fortaleza');
        if(WebConfig::find(1) != null){
            $this->h_increment = json_decode(WebConfig::find(1)->irrigate_interval, true);
        }
    }

    public function viewIndex(){
        $idArray = DB::table('data')->pluck('id');
        $dateArray = DB::table('data')->pluck('dataTime');
        $temperatureArray = DB::table('data')->pluck('temperature');
        $humidityArray = DB::table('data')->pluck('humidity');
        $dataArray_temp = [];
        $dataArray_hum = [];

        for($i = 0; $i < count($idArray); $i++){
            $dataArray_temp[$i] = [$idArray[$i], $temperatureArray[$i], $this->chart_getHTMLtooltip_temp($dateArray[$i], $temperatureArray[$i])];
            $dataArray_hum[$i] = [$idArray[$i], $humidityArray[$i], $this->chart_getHTMLtooltip_hum($dateArray[$i], $humidityArray[$i])];
        }

        $chartTemp_data = json_encode($dataArray_temp);
        $chartHum_data = json_encode($dataArray_hum);

        //dd(date("Y-m-d H:i:s", time()));

        //dd($dateArray, $temperatureArray, $humidityArray);

        return view('index', compact('chartTemp_data', 'chartHum_data'));
    }

    public function statusUpdateEncoded($status)
    {
        $update = json_decode($status, true);
        $time = time();
        $date = date('Y-m-d H:i:s');
        $insertTime = true;
        $this->defineIrrigationInterval($update['interval']);

        if($update['flag'] == 1) $insertTime =  WebConfig::updateOrCreate(['id' => 1],['lastIrrigationTime' => $date]);

        $weather = "";
        $wIntensity = "";

        if($update['weather-state'] == "chuva") {
            $weather = $update['weather-state'];
            if(strtoupper($update['weather-intensity']) == "B") $wIntensity = "Fraca";
            else if(strtoupper($update['weather-intensity']) == "M") $wIntensity = "Moderada";
            else if(strtoupper($update['weather-intensity']) == "A") $wIntensity = "Forte";
            else $wIntensity = "Não Informado";
        }
        else $weather = "limpo";

        $dateInsert = Data::create([
            'temperature' => $update['temperature'],
            'humidity' => $update['humidity'],
            'lumens' => $update['lumens'],
            'soilHumidity' => $update['soil-humidity'],
            'weatherState' => $weather,
            'weatherIntensity' => $wIntensity,
            'dataTime' => $time,
        ]);

        if($dateInsert){
            return response()->json(['insert' => true]);
        }
        else{
            return response()->json(['insert' => false]);
        }
    }

    public function statusUpdate($t, $h, $l, $sh, $w)
    {
        $time = time();
        $insertTime = date('Y-m-d H:i:s');
        $weather = "";

        if($w == "chuva") $weather = $w;
        else $weather = "limpo";

        $dateInsert = Data::create([
            'temperature' => $t,
            'humidity' => $h,
            'lumens' => $l,
            'soilHumidity' => $sh,
            'weatherState' => $weather,
            'dataTime' => $time,
        ]);

        $insert = WebConfig::updateOrCreate(['id' => 1],['lastIrrigationTime' => $insertTime]);

        if($dateInsert && $insert){
            return response()->json(['insert' => true]);
        }
        else{
            return response()->json(['insert' => false]);
        }

    }

    public function statusUpdate2($t, $h, $l, $sh, $w, $wI, $flag)
    {
        $date = date('Y-m-d H:i:s');
        $time = time();
        $insertTime = true;

        if($flag == "1") {$insertTime =  WebConfig::updateOrCreate(['id' => 1],['lastIrrigationTime' => $date]);}

        $weather = "";
        $wIntensity = "";

        if($w == "chuva") {
            $weather = $w;
            if(strtoupper($wI) == "B") $wIntensity = "Fraca";
            else if(strtoupper($wI) == "M") $wIntensity = "Moderada";
            else if(strtoupper($wI) == "A") $wIntensity = "Forte";
            else $wIntensity = "Não Informado";
        }
        else $weather = "limpo";

        $dateInsert = Data::create([
            'temperature' => $t,
            'humidity' => $h,
            'lumens' => $l,
            'soilHumidity' => $sh,
            'weatherState' => $weather,
            'weatherIntensity' => $wIntensity,
            'dataTime' => $time,
        ]);

        if($dateInsert){
            return response()->json(['insert' => true]);
        }
        else{
            return response()->json(['insert' => false]);
        }

    }

    public function getStatus()
    {
        $last = DB::table('data')->orderBy('id', 'desc')->first();
        $lastTime = WebConfig::find(1);

        if($last != null){
            $lastInfo = Data::find($last->id);

            if($lastTime != null && $lastTime->lastIrrigationTime > 0) {
                $lastTime = $lastTime->lastIrrigationTime;
                $lastIrrigation = $this->getTimeArray(explode(" ", $lastTime));
                $nextIrrigation = $this->getNextIrrigationTime(explode(" ", $lastTime));
            }
            else{
                $lastIrrigation = 0;
                $nextIrrigation = 0;
            }

            $lastInfo = [
                'temperature' => $lastInfo->temperature,
                'humidity' => $lastInfo->humidity,
                'lumens' => $lastInfo->lumens,
                'soilHumidity' => $lastInfo->soilHumidity,
                'weatherState' => $lastInfo->weatherState,
                'weatherIntensity' => $lastInfo->weatherIntensity,
                'dataTime' => $lastInfo->dataTime,
                'lastIrrigation' => $lastIrrigation,
                'nextIrrigation' => $nextIrrigation
            ];
             
        }
        else{
            $lastInfo = ['noData' => true];
        }

        $json = json_encode($lastInfo);
        
        return response()->json($json)->header('Access-Control-Allow-Origin' , '*');
    }

    public function setIrrigation($value)
    {
        $boolean = filter_var($value, FILTER_VALIDATE_BOOLEAN);

        $modify = IrrigationState::updateOrCreate(
            ['id' => 1],
            ['irrigate' => $boolean]
        );

        if($modify) return response()->json(['insert' => true])->header('Access-Control-Allow-Origin' , '*');
        else return response()->json(['insert' => false])->header('Access-Control-Allow-Origin' , '*');
    }

    public function isIrrigationTime()
    {
        $irrigation = IrrigationState::find(1);
        $irrigationValue = false;

        if($irrigation != null) $irrigationValue = boolval($irrigation->irrigate);

        if($irrigationValue){
            $timeIrrigate = date('Y-m-d H:i:s');

            $insert = IrrigationState::updateOrCreate(
                ['id' => 1],
                ['irrigate' => false]
            );

            $lastTime = WebConfig::updateOrCreate(['id' => 1],['lastIrrigationTime' => $timeIrrigate]);
        }

        return response()->json(['irrigate' => $irrigationValue]);
    }

    // public function irrigate()
    // {
    //     $hourDB = Data::orderBy('id','desc')->first();
    //     $last = explode(" ", $hourDB->updated_at);

    //     $currentDate = date('Y-m-d H:i:s');
    //     $irrigateDate = $this->incrementHours($last[1], ":", $last[0], "-", $this->h_increment, true);

    //     if(strtotime($currentDate) >= strtotime($irrigateDate)){
    //         $this->setIrrigation('true');
    //         return true;
    //     }
    //     else{
    //         return false;
    //     }
    // }

    public function getNextIrrigationTime($lastTime)
    {
        //$date = explode("-", $lastTime[0]);
        //$hour = explode(":", $lastTime[1]);

        $time = $this->incrementHours($lastTime[1], ":", $lastTime[0], "-", $this->h_increment);

        return $time;
    }

    public function cronIrrigationTime()
    {
        $hourDB = Data::orderBy('id','desc')->first();
        $last = explode(" ", $hourDB->updated_at);
        $dateTimeFormat = $this->incrementHours($last[1], ":", $last[0], "-", $this->h_increment, true, '+3');
        $date = explode("-", explode(" ", $dateTimeFormat)[0]);
        $hour = explode(":", explode(" ", $dateTimeFormat)[1]);

        //dd($dateTimeFormat, $this->h_increment, $last, $date, $hour);

        $cron = $hour[1].' '.$hour[0].' '.$date[2].' '.$date[1].' *';

        return $cron;
    }

    public function defineIrrigationInterval($interval)
    {
        $min = intval($interval)%60;
        $hour = intval(intval($interval)/60);

        $interval = [
            'hI' => $hour,
            'mI' => $min
        ];
        $intervalJSON = json_encode($interval);

        $insert = WebConfig::updateOrCreate(
            ['id' => 1],
            ['irrigate_interval' => $intervalJSON]
        );

        return response()->json(['insert' => boolval($insert)]);
    } 

    // public function defineIrrigationInterval(Request $request){

    // }

    public function incrementHours($baseHour, $hourSeparator, $baseDate = "", $dateSeparator = "", $incrementInHours, $dateTimeFormat = false, $timeZoneOffset = '0')
    {
        if($baseDate != ""){
            $date = explode($dateSeparator, $baseDate);
            $hour = explode($hourSeparator, $baseHour);
            $mounthDays = cal_days_in_month(CAL_GREGORIAN, intval($date[1]), intval($date[0]));
            $temp = intval($hour[1]) + $incrementInHours['mI'];

            if($temp > 59){
                $hour[1] = str_pad($temp - 60, 2, '0', STR_PAD_LEFT);
                $temp = intval($hour[0]) + $incrementInHours['hI'] + intval($timeZoneOffset) + 1;
                if($temp > 23){
                    $hour[0] = str_pad($temp - 24, 2, '0', STR_PAD_LEFT);
                    $temp = intval($date[2]) + 1;
                    if($temp > $mounthDays){
                        $date[2] = str_pad($temp - $mounthDays, 2, '0', STR_PAD_LEFT);
                        $temp = intval($date[1]) + 1;
                        if($temp > 12){
                            $date[1] = str_pad($temp - 12, 2, '0', STR_PAD_LEFT);
                            $date[0] = str_pad(intval($date[0]) + 1, 2, '0', STR_PAD_LEFT);
                        }
                        else{
                            $date[1] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                        }
                    }
                    else{
                        $date[2] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                    }
                }
                else{
                    $hour[0] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                }
            }
            else{
                $hour[1] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                $temp = intval($hour[0]) + $incrementInHours['hI'] + intval($timeZoneOffset);

                if($temp > 23){
                    $hour[0] = str_pad($temp - 24, 2, '0', STR_PAD_LEFT);
                    $temp = intval($date[2]) + 1;
                    if($temp > $mounthDays){
                        $date[2] = str_pad($temp - $mounthDays, 2, '0', STR_PAD_LEFT);
                        $temp = intval($date[1]) + 1;
                        if($temp > 12){
                            $date[1] = str_pad($temp - 12, 2, '0', STR_PAD_LEFT);
                            $date[0] = str_pad(intval($date[0]) + 1, 2, '0', STR_PAD_LEFT);
                        }
                        else{
                            $date[1] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                        }
                    }
                    else{
                        $date[2] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                    }
                }
                else{
                    $hour[0] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                }
            }

            $time = [
                'date' => $date[2].'/'.$date[1],
                'hour' => $hour[0].':'.$hour[1]
            ];

            $formatDT = $date[0].'-'.$date[1].'-'.$date[2].' '.$hour[0].':'.$hour[1].':'.$hour[2];
        }
        else{
            $hour = explode($hourSeparator, $baseHour);
            $temp = intval($hour[1]) + $incrementInHours['mI'];
            
            if($temp > 59){
                $hour[1] = str_pad($temp - 60, 2, '0', STR_PAD_LEFT);
                $temp = intval($hour[0]) + $incrementInHours['hI'] + intval($timeZoneOffset) + 1;
                if($temp > 23){
                    $hour[0] = str_pad($temp - 24, 2, '0', STR_PAD_LEFT);
                }
                else{
                    $hour[0] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                }
            }
            else{
                $hour[1] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                $temp = intval($hour[0]) + $incrementInHours['hI'] + intval($timeZoneOffset);

                if($temp > 23){
                    $hour[0] = str_pad($temp - 24, 2, '0', STR_PAD_LEFT);
                }
                else{
                    $hour[0] = str_pad($temp, 2, '0', STR_PAD_LEFT);
                }
            }

            $time = $hour[0].':'.$hour[1];
        }

        if($dateTimeFormat) return $formatDT;

        return $time;
    }

    public function getTimeArray($time)
    {
        $date = explode("-", $time[0]);
        $hour = explode(":", $time[1]);

        $time = [
            'date' => $date[2].'/'.$date[1],
            'hour' => $hour[0].':'.$hour[1]
        ];

        return $time;
    }

    public function chart_getHTMLtooltip_temp($date, $temperature)
    {
        $d = date('d/m/Y', $date);
        $h = date('H:i:s', $date);
        $dateComplete = $d.' <b>às</b> '.$h; 

        $html = '<div style="padding:5px 5px 5px 5px; font-size: 12pt;">';
        $html .= '<p><b>Data:</b> &nbsp;'.$dateComplete.'</p>';
        $html .= '<p><b>Temperatura:</b> &nbsp;'.$temperature.'<b>°C</b></p>';
        $html .= '</div>';

        return $html;
    }

    public function chart_getHTMLtooltip_hum($date, $humidity)
    {
        $d = date('d/m/Y', $date);
        $h = date('H:i:s', $date);
        $dateComplete = $d.' <b>às</b> '.$h;

        $html = '<div style="padding:5px 5px 5px 5px; font-size: 12pt;">';
        $html .= '<p><b>Data:</b> &nbsp;'.$dateComplete.'</p>';
        $html .= '<p><b>Umidade:</b> &nbsp;'.$humidity.'<b>%</b></p>';
        $html .= '</div>';

        return $html;
    }
}
