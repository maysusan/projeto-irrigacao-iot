let interval = Math.floor((Math.random() * 30) + 31);

function enviaSTATUS()
{
    var date = new Date();
    var weather = ['chuva', 'limpo'];
    var wIntensity = ['B', 'M', 'A'];
    var flag = Math.floor((Math.random() * 2));
    var rand = weather[Math.floor((Math.random() * 2))];
    var rand2 = wIntensity[Math.floor((Math.random() * 3))];
    var JSONvar = {
        'temperature': Math.floor((Math.random() * 30) + 18),
        'humidity': Math.floor((Math.random() * 70) + 10),
        'lumens': Math.floor((Math.random() * 4900) + 100),
        'soil-humidity': Math.floor((Math.random() * 50) + 10),
        'weather-state': rand,
        'weather-intensity': rand2,
        'flag': flag,
        'interval': interval,
    };

    $.ajax({
        'url' : 'http://18.188.214.246/semb/public/statusUpdate='+encodeURIComponent(JSON.stringify(JSONvar)),
        type : 'get',
        dataType: 'json',
        contentType: 'application/json',
        crossDomain: true,
        timeout: 1000,
        beforeSend : function(){}
    })
    .done(function(msg){
        console.log('Status Atual Enviado!');
    })
    .fail(function(jqXHR, textStatus, msg){
        console.log("Erro na requisição! Erro -> "+textStatus);
        console.log("Envio:");
        console.log(JSONvar);
        console.log("URL: "+"http://18.188.214.246/semb/public/statusUpdate="+encodeURIComponent(JSON.stringify(JSONvar)));
    });
}

function enviaIRRIGAR()
{
    $.ajax({
        'url' : 'http://18.188.214.246/semb/public/irrigationUpdate=true',
        type : 'get',
        dataType: 'json',
        contentType: 'text/plain',
        crossDomain: true,
        timeout: 1000,
        beforeSend : function(){}
    })
    .done(function(msg){
    })
    .fail(function(jqXHR, textStatus, msg){
        console.log("Erro na requisição de mandar Irrigar! Erro -> "+textStatus);
    });
}

function irrigar()
{
    $.ajax({
        'url' : 'http://18.188.214.246/semb/public/irrigationStatus',
        type : 'get',
        dataType: 'json',
        contentType: 'text/plain',
        crossDomain: true,
        timeout: 1000,
        beforeSend : function(){}
    })
    .done(function(msg){
        console.log('Irrigou? : '+msg.irrigate);
    })
    .fail(function(jqXHR, textStatus, msg){
        console.log("Erro na requisição de Irrigar! Erro -> "+textStatus);
    });
}