var msgView = false;
var response = null;

function atualizaStatus(){
    $.ajax({
        'url' : 'http://18.188.214.246/semb/public/status',
        type : 'get',
        dataType: 'json',
        crossDomain: true,
        timeout: 1000,
        beforeSend : function(){}
    })
    .done(function(msg){
        var resposta = JSON.parse(msg);
        response = resposta.noData;

        if(resposta.noData != true){
            $("#temperature").html(resposta.temperature+" °C");
            lastTemp = resposta.temperature;
            $("#humidity").html(resposta.humidity+"%");
            $("#lumen").html(resposta.lumens+" lúmens");
            $("#soil-humidity").html(resposta.soilHumidity+"%");
            if(resposta.lastIrrigation != 0){
                $('#last-irr').html(resposta.lastIrrigation.date+" <i>às</i> "+resposta.lastIrrigation.hour+"h");
                $('#next-irr').html(resposta.nextIrrigation.date+" <i>às</i> "+resposta.nextIrrigation.hour+"h");
            }
            else{
                $('#last-irr').html("Não foi feita nenhuma irrigação!");
                $('#next-irr').html("Não foi feita nenhuma irrigação!");
            }
            if(resposta.weatherState == "limpo"){
                $('#rStatus').html("Céu limpo");
                $('#pIntensity').addClass('hidden');
            }
            else{
                $('#rStatus').html("Chovendo");
                $('#pIntensity').removeClass('hidden');
                $('#rIntensity').html(resposta.weatherIntensity);
            }
        }
        else{
            if(!msgView){
                msgView = true;
            }
            lastTemp = 0;
        }

        document.getElementById('temperature-content').innerHTML = "";
        anyChartTemperature();

    })
    .fail(function(jqXHR, textStatus, msg){
        console.log("Erro na requisição! Erro -> "+textStatus);
    });
}

////////////// GOOGLE CHART SCRIPT

let options, chart, data;
let options_hum, chart_hum, data_hum;

google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(setDataAndChart);

function setDataAndChart() {
    data = new google.visualization.DataTable();
    data_hum = new google.visualization.DataTable();

    data.addColumn('number', 'Data ID');
    data.addColumn('number', 'Temperatura (°C)');
    data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

    data_hum.addColumn('number', 'Data ID');
    data_hum.addColumn('number', 'Umidade (%)');
    data_hum.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

    data.addRows(chartData_temp);
    data_hum.addRows(chartData_hum);

    options = {
        animation: {
            startup: true,   /* Need to add this for animations */
            duration: 800,
            easing: 'out',
        },
        curveType: 'function',
        pointSize: 1,
        hAxis: {
            title: 'Data ID',
        },
        vAxis: {
            title: 'Temperatura (°C)',
            format:'#,###°C',
        },
        legend: {
            position: 'top',
            alignment: 'center',
        },
        colors: ['#1100FF'],
        explorer: {
            actions: ['dragToZoom', 'rightClickToReset'],
            axis: 'horizontal',
        },
        'width': document.getElementById('bodyOfModal').innerWidth,
        'height': 400,
        orientation: 'horizontal',
        tooltip: { isHtml: true },
    };

    options_hum = {
        animation: {
            startup: true,   /* Need to add this for animations */
            duration: 800,
            easing: 'out',
        },
        curveType: 'function',
        pointSize: 1,
        hAxis: {
            title: 'Data ID',
        },
        vAxis: {
            title: 'Umidade (%)',
            maxValue: 100,
        },
        legend: {
            position: 'top',
            alignment: 'center',
        },
        colors: ['#03ACFF'],
        explorer: {
            actions: ['dragToZoom', 'rightClickToReset'],
            axis: 'horizontal',
        },
        'width': document.getElementById('bodyOfModal').innerWidth,
        'height': 400,
        orientation: 'horizontal',
        tooltip: { isHtml: true },
    };

    chart = new google.visualization.LineChart(document.getElementById('chart_div_temp'));
    chart_hum = new google.visualization.LineChart(document.getElementById('chart_div_hum'));
}

function getCoords() {
    var chartLayout = chart.getChartLayoutInterface();
    var chartBounds = chartLayout.getChartAreaBoundingBox();
    return {
      x: {
        min: chartLayout.getHAxisValue(chartBounds.left),
        max: chartLayout.getHAxisValue(chartBounds.width + chartBounds.left)
      },
      y: {
        min: chartLayout.getVAxisValue(chartBounds.top),
        max: chartLayout.getVAxisValue(chartBounds.height + chartBounds.top)
      }
    };
}
function setRange(coords) {
    options.hAxis = {};
    options.vAxis = {};
    options.hAxis.viewWindow = {};
    options.vAxis.viewWindow = {};
    if (coords) {
      options.hAxis.viewWindow.min = coords.x.min;
      options.hAxis.viewWindow.max = coords.x.max;
      options.vAxis.viewWindow.min = coords.y.min;
      options.vAxis.viewWindow.max = coords.y.max;
    }
}

function clickMouse(){
    options.animation = {};
    chart.draw(data, options);

    options_hum.animation = {};
    chart_hum.draw(data_hum, options_hum);
}

/////////////////////////////////////

function checaOrientacao(){
    if(window.innerHeight > window.innerWidth){
        options.width = document.getElementById('bodyOfModal').innerWidth;
        options.height = 800;
        options.orientation = 'vertical';

        options_hum.width = document.getElementById('bodyOfModal').innerWidth;
        options_hum.height = 800;
        options_hum.orientation = 'vertical';
    }
    else{
        options.width = document.getElementById('bodyOfModal').innerWidth;
        options.height = 400;
        options.orientation = 'horizontal';

        options_hum.width = document.getElementById('bodyOfModal').innerWidth;
        options_hum.height = 400;
        options_hum.orientation = 'horizontal';
    }
}

function checaGrafico(){
    checaOrientacao();

    options.animation = {
        startup: true,
        duration: 800,
        easing: 'out',
    };
    chart.draw(data, options);

    options_hum.animation = {
        startup: true,
        duration: 800,
        easing: 'out',
    };
    chart_hum.draw(data_hum, options_hum);
}

function checagem1(){
    if(window.innerWidth <= 991){
        $('#info1_div').removeClass('flex-row').addClass('flex-column');
        $('#temperature-div').css('margin-left', '50px');
        if(window.innerWidth <= 767){
            $('#info2_div').removeClass('flex-row').addClass('flex-column');
            $('#rain-status-div').css("margin-bottom", "20px");
            if(window.innerWidth <= 575){
                $('#humidity-div').css("margin-bottom", "20px");
                $('#lumen-div').css("margin-bottom", "20px");
                $('#soil-humidity-div').css("margin-bottom", "20px");
                $('#temperature-div').css("margin-bottom", "20px");
                $('#system-info').removeClass('grid-container').addClass('flex-container').addClass('flex-column');
                $('#temperature-div').css('margin-left', '0px');
            }
            else{
                $('#humidity-div').css("margin-bottom", "0px");
                $('#lumen-div').css("margin-bottom", "0px");
                $('#soil-humidity-div').css("margin-bottom", "0px");
                $('#temperature-div').css("margin-bottom", "0px");
                $('#system-info').removeClass('flex-container').removeClass('flex-column').addClass('grid-container');
                $('#temperature-div').css('margin-left', '50px');
            }
        }
        else{
            $('#rain-status-div').css("margin-bottom", "0px");
            $('#info2_div').removeClass('flex-column').addClass('flex-row');
        }
    }
    else{
        $('#info1_div').removeClass('flex-column').addClass('flex-row');
        $('#temperature-div').css('margin-left', '0px');
    }
}

$('#historic-modal').on('show.bs.modal', function () {
    if(lastTemp == 0){
        alert('Não há dados inseridos!')
    }
    else{
        setTimeout(function(){
            checaGrafico();
        }, 230);
    }
});

$(window).on('resize', function(){
    checaGrafico();
    checagem1();
});

$(document).ready(function(){
    checagem1();
    if(response == true){
        alert('Não há dados no sistema!');
    }
    atualizaStatus();
    setInterval(function () {
        atualizaStatus();
    }, 3000);
});