<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProjectController@viewIndex')->name('index');

Route::get('statusUpdate={status}', 'ProjectController@statusUpdateEncoded')->name('update.status.encoded');

Route::get('statusUpdate&temp={t}&hum={h}&lum={l}&soilHum={sh}&weather={w}', 'ProjectController@statusUpdate')->name('update.status');

Route::get('statusUpdate2&temp={t}&hum={h}&lum={l}&soilHum={sh}&weather={w}&wIntensity={wI}&flag={flag}', 'ProjectController@statusUpdate2')->name('update.status2');

Route::get('status', 'ProjectController@getStatus')->name('get.status');

Route::get('irrigationUpdate={value}', 'ProjectController@setIrrigation')->name('update.status.irrigate');

Route::get('irrigationStatus', 'ProjectController@isIrrigationTime')->name('status.irrigate');

Route::get('interval={interval}', 'ProjectController@defineIrrigationInterval' )->name('set.irrigate.interval');
